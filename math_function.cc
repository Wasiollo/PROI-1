#include "math_function.hh"
#include <cmath>
#include <algorithm>


double grad_to_rad (double x)
{
    double y ;
    y=x*M_PI/200 ;
    return y;
}
//this function change grads to radians

double deg_to_rad (double x)
{
    double y ;
    y=x*M_PI/180 ;
    return y;
}
//this function change degrees to radians

double middle (double x1, double x2)
{
	return (x1+x2)/2;
}
// arithmetic average

double wsp_a (double x1, double y1, double x2, double y2)
{
	return ((y2-y1)/(x2-x1)) ;
}
// coefficient "a" of the line connecting points x1,y1 and x2,y2

double wsp_b (double x1, double y1, double a)
{
	return (y1-x1*a) ;
}
// coefficient "b" of the line connecting points x1,y1 and x2,y2

double segment_lenth(double ax, double ay, double bx, double by)
{
    double a;
    a=sqrt((bx-ax)*(bx-ax)+(by-ay)*(by-ay)) ;
    return a ;
}
// length of the segment with given coordinates in 2 dimensions

double heron (double a,double b,double c)
{
        double p; //wspolczynnik p do herona
        double POLE;
        p=(a+b+c)/2 ;
        POLE=sqrt(p*(p-a)*(p-b)*(p-c));
        return POLE ;
}
// Area of triangle (heron)

void meet_point (double& x, double& y,double a1, double b1, double a2, double b2)
{
    x=(b2-b1)/(a1-a2);
    y=a1*x+b1;
}
// x and y get the values where lines y1=a1*x+b1 and y2=a2*x+b2 cross

bool collinear(double ax, double ay, double bx, double by, double x, double y)
{
   double a, b ;
   a=wsp_a(ax,ay,bx,by);
   b=wsp_b(ax,ay,a);
   return y==a*x+b ;
}
// checking if point (x,y) is collinear with line y=ax+b

bool numerical_interval(double& x_beg, double& x_end, double x1, double x2, double x3, double x4)
{
    double max_1,max_2,min_1,min_2;
    max_1=std::max(x1,x2);
    min_1=std::min(x1,x2);
    max_2=std::max(x3,x4);
    min_2=std::min(x3,x4);

    if(max_1<min_2 || max_2<min_1)
        return false;

    if(max_2>max_1)
    {
        if(min_2>min_1)
        {
            x_beg=min_2;
            x_end=max_1;
        }
        else
        {
            x_beg=min_1;
            x_end=max_1;
        }
    }
    else
    {
        if(min_1>min_2)
        {
            x_beg=min_1;
            x_end=max_2;
        }
        else
        {
            x_beg=min_2;
            x_end=max_2;
        }
    }
    return true ;

}
// if true, there is interval which contain
// a part of this intervals x_beg is this interval begin
// x_end is end of this interval
// if false there is no such interval

bool inside_interval(double x, double a, double b)
{
    if(x>=a&&x<=b)
        return true;
    else
        return false;
}
// check if x is between a and b


