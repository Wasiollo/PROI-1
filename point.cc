#include "point.hh"
#include <iostream>
#include <iomanip>

void Point::move_point(double z, double w)
{
    x+=z;
    y+=w;
}

void Point::set_point (double z, double w)
{
    x=z;
    y=w;
}

void Point::print_point()
{

    std::cout<<std::fixed<<std::setprecision(6)<<"X: "<<x<<" Y: "<<y<<std::endl;
}

double Point::return_x()
{
    return x;
}

double Point::return_y()
{
    return y;
}
