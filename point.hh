#ifndef POINT_HH_INCLUDED
#define POINT_HH_INCLUDED

class Point
{
    private:
    double x,y;
    public:
    Point(void)
        {
            x=0;
            y=0;
        }
    Point (double z,double w)
        {
            x=z ;
            y=w ;
        }

        double return_x();
        double return_y();
        void move_point(double z, double w) ;
        void set_point (double z, double w) ;
        void print_point();
};
#endif // POINT_HH_INCLUDED
