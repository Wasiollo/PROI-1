#ifndef MATH_FUNCTION_HH_INCLUDED
#define MATH_FUNCTION_HH_INCLUDED

double grad_to_rad (double x);//this function change grads to radians
double deg_to_rad (double x); //this function change degrees to radians
double middle (double x1, double x2) ; // arithmetic average
double wsp_a (double x1, double y1, double x2, double y2) ; // coefficient "a" of the line connecting points x1,y1 and x2,y2
double wsp_b (double x1, double y1, double a) ; // coefficient "b" --------//---------------------------------------
double segment_lenth(double ax, double ay, double bx, double by) ; // length of the segment with given coordinates in 2 dimensions
double heron (double a,double b,double c); // area of triangle (heron)
void meet_point(double& x, double& y,double a1, double b1, double a2, double b2) ; // x and y get the values where lines y1=a1*x+b1 and y2=a2*x+b2 cross
bool collinear (double ax, double ay, double bx, double by, double x, double y); // checking if point (x,y) is collinear with line y=ax+b
bool numerical_interval(double& x_beg, double& x_end, double x1, double x2, double x3, double x4);// if true, there is interval which contain
                                                                                                  // a part of this intervals x_beg is this interval
                                                                                                  // begin x_end is end of this interval
                                                                                                  // if false there is no such interval
bool inside_interval(double x, double a, double b);// check if x is between a and b

#endif // MATH_FUNCTION_HH_INCLUDED
