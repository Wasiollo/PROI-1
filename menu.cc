#include "menu.hh"
#include <iostream>
#include <cmath>
#include "quadrangle.hh"
#include "basic_fun.hh"
#include "math_function.hh"

void Menu::show ()
{
    int ile;
    std::cout<<"Witaj w programie edycji czworokata."<<std::endl ;
    std::cout<<"Podaj ile czworokatow bedziesz chcial edytowac."<<std::endl;
    std::cin>>ile;
    Quadrangle kw[ile];
    int x;
    int i=0 ;
    clrscr();
    while (1)
    {
        std::cout<<"Witaj w programie edycji czworokata. \t Aktualnie edytujesz czworokat "<<i+1<<std::endl ;
        std::cout<<"Co chcesz zrobic?"<<std::endl;
        std::cout<<"0. Wybrac czworokat na ktorym beda wykonywane operacje."<<std::endl;
        std::cout<<"1. Ustawic/edytowac dane czworokata."<<std::endl;
        std::cout<<"2. Przesunac czworokat."<<std::endl ;
        std::cout<<"3. Obrocic czworokat."<<std::endl;
        std::cout<<"4. Skalowac czworokat."<<std::endl;
        std::cout<<"5. Obliczyc obwod czworokata. "<<std::endl;
        std::cout<<"6. Obliczyc pole czworokata. "<<std::endl;
        std::cout<<"7. Symetria wzgledem prostej. "<<std::endl;
        std::cout<<"8. Wspolne punkty dwoch czworokatow. (Nie dziala) "<<std::endl ;
        std::cout<<"9. Wypisac koordynanty punktow"<<std::endl ;
        std::cout<<"10. Zakoncz program"<<std::endl ;
        std::cout<<"11. TEST "<<std::endl;
        std::cin>> x;

        switch (x)
        {
            case 0 :
                int j;
                clrscr();
                std::cout<<"Wybor czworokata, na ktorym beda wkonywane operacje."<<std::endl<<std::endl;
                std::cout<<"Podaj numer czworokata do edycji (liczby od 1 do "<<ile<<") i zatwierdz enterem."<<std::endl;
               // do {
                std::cin>>j;
                i=j-1;
                //} // edytuj
                //while(i>=ile);
                std::cout<<"Wybrano czworokat "<<j<<std::endl;
                pause();
                clrscr();
                break;
            case 1 :
                clrscr();
                double a,b,c,d,e,f,g,h;
                bool is_ok ;
                is_ok=true ;
                std::cout<<"Wprowadz koordynanty punktow (8 liczb)"<<std::endl ;
                std::cout<<"Punkty podajemy w kolejnosci."<<std::endl;
                std::cout<<"Kazdy punkt jako dwie wspolrzedne: X i Y, odzielone spacjami."<<std::endl;
                std::cout<<"Kazda pare zatwierdzamy enterem."<<std::endl;
                do
                {
                    if(is_ok==false)
                        std::cout<<"Podaj poprawne dane"<<std::endl;
                    std::cin>>a>>b>>c>>d>>e>>f>>g>>h;
                    bufor_clear();
                    is_ok=kw[i].set_quadrangle(a,b,c,d,e,f,g,h);
                }
                while (is_ok==false) ;

                std::cout<<"Ustawiono punkty"<<std::endl;
                pause() ;
                clrscr() ;
                break ;
            case 2 :
                clrscr();
                std::cout << "O ile chcesz go przesunac? Podaj dwie liczby X i Y"<<std::endl;
                std::cout << "Czworokat zostanie przesyniety o X i Y w ukladzie kartezjanskim"<<std::endl;
                double y,z;
                std::cin >> y >> z;
                kw[i].move_quadrangle(y,z) ;
                pause() ;
                clrscr() ;
                break ;
            case 3:
                clrscr();
                char check;
                double fi;
                std::cout<< "Obracanie czworokata."<<std::endl<<std::endl;
                std::cout<< "Jesli chcesz podac kat w:"<<std::endl;
                std::cout<<"1. stopniach nacisnij ' d ' i zatwierdz enterem"<<std::endl;
                std::cout<<"2. radianach nacisnij ' r ' i zatwierdz enterem"<<std::endl;
                std::cout<<"3. gradach nacisnij ' g ' i zatwierdz eneterem"<<std::endl;
                std::cin>>check;
                std::cout<<std::endl<<"Teraz podaj wartosc kata, o jaki chcesz obrocic czworokata i zatwierdz eneterem"<<std::endl;
                std::cin >> fi;
                switch (check)
                {
                    case 100: //d in ascii
                        fi=deg_to_rad(fi);
                        break;
                    case 103: //g in ascii
                        fi=grad_to_rad(fi);
                        break;
                    case 114: //r in ascii
                        break;
                    default:
                        std::cout<<"error"<<std::endl;
                }

                kw[i].quadrangle_rotation(fi);
                std::cout<<"obrocono"<<std::endl ;
                pause() ;
                clrscr();
                break;
            case 4:
                clrscr();
                std::cout<< "jaki wspolczynnik skalowania?"<<std::endl;
                double l;
                std::cin >> l;
                kw[i].graduation_quadrangle(l);
                std::cout<<"przeskalowano"<<std::endl;
                pause() ;
                clrscr();
                break;
            case 5:
                clrscr();
                std::cout<< "Obliczony obwod czworokata:"<<std::endl;
                std::cout<<kw[i].quadrangle_circuit()<<std::endl;
                pause();
                clrscr();
                break ;
            case 6:
                clrscr() ;
                std::cout<<"Obliczone pole czworokata:"<<std::endl;
                std::cout<<kw[i].quadrangle_area()<<std::endl;
                pause();
                clrscr() ;
                break;
            case 7:
                clrscr() ;
                std::cout<<"Podaj wspolczynniki a i b prostej o rownaniu y=ax+b"<<std::endl ;
                double wsp_a,wsp_b;
                std::cin >> wsp_a>>wsp_b ;
                kw[i].quadrangle_line_symmetry(wsp_a,wsp_b);
                pause();
                clrscr();
                break;
            case 8:
                clrscr();
                int x;
                std::cout<<"Punkty wspolne dwoch czwrokokatow"<<std::endl<<std::endl;
                std::cout<<"Chcesz podac dane jakiegos czworokata"<<std::endl;
                std::cout<<"czy wybrac jeden z innych istniejacych czworokatow,"<<std::endl;
                std::cout<<"aby znalezc punkty wspolne?"<<std::endl<<std::endl;
                std::cout<<"Nacisnij i zatwierdz enterem:"<<std::endl;
                std::cout<<"1 jesli chcesz podac dane czworokata"<<std::endl;
                std::cout<<"2 jesli chcesz podac indeks istniejacego juz czworokata"<<std::endl;
                std::cin>>x;
                if(x==1)
                {
                    Quadrangle kw2;
                    std::cout<<"Podaj dane czworokata"<<std::endl;
                    do
                    {
                        if(is_ok==false)
                        std::cout<<"POdaj poprawne dane"<<std::endl;
                        std::cin>>a>>b>>c>>d>>e>>f>>g>>h;
                        bufor_clear();
                        is_ok=kw2.set_quadrangle(a,b,c,d,e,f,g,h);
                    }
                    while (is_ok==false) ;

                    kw[i].common_points(kw2);
                }
                if(x==2)
                {
                    int l;
                    std::cout<<"Podaj indeks czworokata z ktorym chcesz porownac czworokat "<<i<<std::endl;
                    std::cout<<"Mozesz podac liczbe z zaresu od 1 do "<<ile<<std::endl<<std::endl;
                    std::cin>> l;
                    l--;
                    if(i==l)
                    {
                        std::cout<<"Wybrano ten sam czworokat"<<std::endl;
                    }
                    else
                        kw[i].common_points(kw[l]);
                }
                pause() ;
                clrscr() ;
                break ;
            case 9 :
                clrscr();
                std::cout <<"Koordynanty czworokata:"<<std::endl ;
                kw[i].print_quadrangle();
                pause() ;
                clrscr() ;
                break ;
            case 10:
                return ;
            case 11:
                clrscr();
                std::cout<<"TESTOWANIE"<<std::endl<<std::endl;
                test();
                pause();
                clrscr();
            default:
                clrscr();
        }
    }
}

void Menu::test()
{

    Quadrangle test1,test2,test3,test4;
    if(test1.set_quadrangle(0,0,0,3,3,3,3,0)==false)
        std::cout<<"test1 bledne dane"<<std::endl;
    if(test2.set_quadrangle(-0.7,1.5,1.5,3.7,3.7,1.5,1.5,-0.7)==false)
        std::cout<<"test2 bledne dane"<<std::endl;
    if(test3.set_quadrangle(1,1,1,2,2,2,2,1)==false)
        std::cout<<"test3 bledne dane"<<std::endl;
    if(test4.set_quadrangle(0,0.1,0,1,3,3.5,4,-41)==false)
        std::cout<<"test4 bledne dane"<<std::endl;
    std::cout<<"test 1"<<std::endl;
    test1.print_quadrangle();
    std::cout<<"test 2"<<std::endl;
    test2.print_quadrangle();
    std::cout<<"test 3"<<std::endl;
    test3.print_quadrangle();
    std::cout<<"test 4"<<std::endl;
    test4.print_quadrangle();
    std::cout<<"test 1 wspolne punkty i test 2"<<std::endl;
    test1.common_points(test2);
    std::cout<<"test 1 wspolne punkty i test 3"<<std::endl;
    test1.common_points(test3);
    std::cout<<"test 1 wspolne punkty i test 4"<<std::endl;
    test1.common_points(test4);
    std::cout<<"test 2 wspolne punkty i test 3"<<std::endl;
    test2.common_points(test3);
    std::cout<<"test 2 wspolne punkty i test 4"<<std::endl;
    test2.common_points(test4);
    std::cout<<"test 3 wspolne punkty i test 4"<<std::endl;
    test3.common_points(test4);

    std::cout<<std::endl<<"czworokat test1"<<std::endl;
    test1.print_quadrangle();
    test1.move_quadrangle(1,1);
    test1.print_quadrangle();
    test1.graduation_quadrangle(2);
    std::cout<<std::endl<<"skalowanie o wspolczynniku 2"<<std::endl ;
    test1.print_quadrangle();
    std::cout<<std::endl<<"obracanie o 90 stopni"<<std::endl ;
    test1.quadrangle_rotation(M_PI/2);
    test1.print_quadrangle();
    std::cout<<"Obwod TEST 1: "<<test1.quadrangle_circuit()<<std::endl;
    std::cout<<"POLE TEST 1: "<<test1.quadrangle_area()<<std::endl;
    test1.quadrangle_line_symmetry(0,0);
    std::cout<<"Symetria TEST 1 wzgledem prostej y=ax+b o wspolczynnikach 0 0"<<std::endl;
    test1.print_quadrangle();

     std::cout<<std::endl<<"czworokat test2"<<std::endl;
    test2.print_quadrangle();
    test2.move_quadrangle(3,8);
    test2.print_quadrangle();
    test2.graduation_quadrangle(4);
    std::cout<<std::endl<<"skalowanie o wspolczynniku 4"<<std::endl ;
    test2.print_quadrangle();
    std::cout<<std::endl<<"obracanie o 180 stopni"<<std::endl ;
    test2.quadrangle_rotation(M_PI);
    test2.print_quadrangle();
    std::cout<<"Obwod TEST 2: "<<test2.quadrangle_circuit()<<std::endl;
    std::cout<<"POLE TEST 2: "<<test2.quadrangle_area()<<std::endl;
    test2.quadrangle_line_symmetry(1,2);
    std::cout<<"Symetria TEST 2 wzgledem prostej y=ax+b o wspolczynnikach 1 2"<<std::endl;
    test2.print_quadrangle();

     std::cout<<std::endl<<"czworokat test 3"<<std::endl;
    test3.print_quadrangle();
    test3.move_quadrangle(2,2);
    test3.print_quadrangle();
    test3.graduation_quadrangle(1.5);
    std::cout<<std::endl<<"skalowanie o wspolczynniku 1.5"<<std::endl ;
    test3.print_quadrangle();
    std::cout<<std::endl<<"obracanie o 270 stopni"<<std::endl ;
    test3.quadrangle_rotation((M_PI/2)*3);
    test3.print_quadrangle();
    std::cout<<"Obwod TEST 3: "<<test3.quadrangle_circuit()<<std::endl;
    std::cout<<"POLE TEST 3: "<<test3.quadrangle_area()<<std::endl;
    test3.quadrangle_line_symmetry(1,0);
    std::cout<<"Symetria TEST 3 wzgledem prostej y=ax+b o wspolczynnikach 1 0"<<std::endl;
    test3.print_quadrangle();

    std::cout<<std::endl<<"czworokat test 4"<<std::endl;
    test4.print_quadrangle();
    test4.move_quadrangle(7,7);
    test4.print_quadrangle();
    test4.graduation_quadrangle(10);
    std::cout<<std::endl<<"skalowanie o wspolczynniku 10"<<std::endl ;
    test4.print_quadrangle();
    std::cout<<std::endl<<"obracanie o 3 radiany"<<std::endl ;
    test4.quadrangle_rotation(3);
    test4.print_quadrangle();
    std::cout<<"Obwod TEST 4: "<<test4.quadrangle_circuit()<<std::endl;
    std::cout<<"POLE TEST 4: "<<test4.quadrangle_area()<<std::endl;
    test4.quadrangle_line_symmetry(-8,17);
    std::cout<<"Symetria TEST 4 wzgledem prostej y=ax+b o wspolczynnikach -8 17"<<std::endl;
    test4.print_quadrangle();






}















