#include "basic_fun.hh"
#include <iostream>
#include <cstdio>
#include <cstdlib>

void bufor_clear()
{
	int c ;
	while((c = getchar()) != '\n' );
}

void clrscr()
{
    #ifdef __linux__
    system("clear");
    #else
    system("cls");
    #endif // __linux__
}

void pause()
{
    std::cin.clear();
    std::cin.ignore();
    std::cin.ignore();
}
