#ifndef QUADRANGLE_HH_INCLUDED
#define QUADRANGLE_HH_INCLUDED

#include "point.hh"

class Quadrangle
{

Point A,B,C,D,mid;

    bool are_points_alrigth(Point& a, Point& b, Point& c, Point& d); // check if points are ok.
    bool any_collinear(Point& a,Point& b, Point& c, Point& d) ;  //this function check if any points are collinear
    bool are_lines_crossed (Point& a, Point& b, Point& c, Point& d) ; //check if segments of quadrangle are crossed
    void point_rotation(Point &a, Point &b, double fi);//rotating point around second point
    void center (); // set center of gravity
    void graduation_point (Point &a, Point &b, double l) ;//graduation point terms to other point
    void one_point_symmetry(double& ax, double& ay,double a, double b, double a_pom);//symmetry of one point terms to the line
    bool crossing_segments_points(double& x, double& y, Point& a, Point& b, double cx, double cy, double dx, double dy);//crosing points of two segments
    void one_segment_crossing(Point& a, Point& b, double ax2, double ay2, double bx2, double by2, double cx2, double cy2, double dx2, double dy2) ;// crossed points of one segment of first quadrangle with all segments of second quadrangle
    void quadrangle_special_symmetry(double b); // symmetry when a=0

public:
    void get_points(double& ax, double& ay, double& bx, double& by, double& cx, double& cy, double& dx, double& dy);//qetting variable of points
    void move_quadrangle(double x,double y) ; // moving whole quadrangle
    void print_quadrangle () ; //printing points of quadrangle
    bool set_quadrangle(double a, double b, double c, double d, double e, double f, double g, double h) ; // setting quadrangle
	void quadrangle_rotation (double fi);//rotating quadrangle around center of gravity
    void graduation_quadrangle(double l);//graduation quadrangle terms to center of gravity
    double quadrangle_circuit (); //circuit of quadrangle
    double quadrangle_area (); //area of quadrangle
    void quadrangle_line_symmetry(double a, double b); //symmetry terms to the line y=ax+b
    void common_points(Quadrangle &kw2); // common points of Quadrangle for which is used this function and Quadrangle kw2.
};

#endif // QUADRANGLE_HH_INCLUDED
