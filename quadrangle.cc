#include "quadrangle.hh"
#include <iostream>
#include <iomanip>
#include <cmath>
#include "math_function.hh"
#include "menu.hh"
#include "basic_fun.hh"

bool Quadrangle::any_collinear(Point& a,Point& b, Point& c, Point& d)
{
    bool c1,c2,c3,c4;
    double ax,ay,bx,by,cx,cy,dx,dy;
    get_points(ax,ay,bx,by,cx,cy,dx,dy);
    //if(ax==bx==cx==dx && ay==by==cy==dy)
      //  return true;
    c1=collinear(ax,ay,bx,by,cx,cy);
    c2=collinear(bx,by,cx,cy,dx,dy);
    c3=collinear(cx,cy,dx,dy,ax,ay);
    c4=collinear(dx,dy,ax,ay,bx,by);
    if(c1 || c2 || c3 || c4)
        return true;
        return false;
}

bool Quadrangle::are_lines_crossed(Point& a, Point& b, Point& c, Point& d)
{
    double ax,bx,cx,dx,ay,by,cy,dy;
    get_points(ax,ay,bx,by,cx,cy,dx,dy);

    double wsp_a1,wsp_b1,wsp_a2,wsp_b2;
    double x,y;

    double x_beg,x_end,y_beg,y_end;
    bool is_interval_1,is_interval_2;

    is_interval_1=numerical_interval(x_beg,x_end,ax,bx,cx,dx);
    is_interval_2=numerical_interval(y_beg,y_end,ay,by,cy,dy);

    if(is_interval_1==1 && is_interval_2==1)
    {
        wsp_a1=wsp_a(ax,ay,bx,by);
        wsp_b1=wsp_b(ax,ay,wsp_a1);
        wsp_a2=wsp_a(cx,cy,dx,dy);
        wsp_b2=wsp_b(cx,cy,wsp_a2);

        meet_point(x,y,wsp_a1,wsp_b1,wsp_a2,wsp_b2);

        double is_inside_interval_1,is_inside_interval_2;
        is_inside_interval_1=inside_interval(x,x_beg,x_end);
        is_inside_interval_2=inside_interval(y,y_beg,y_end);

        if(is_inside_interval_1 && is_inside_interval_2)
            return 1;
    }

    is_interval_1=numerical_interval(x_beg,x_end,ax,dx,bx,cx);
    is_interval_2=numerical_interval(y_beg,y_end,ay,dy,by,cy);

    if(is_interval_1==true && is_interval_2==true)
    {
        wsp_a1=wsp_a(ax,ay,dx,dy);
        wsp_b1=wsp_b(ax,ay,wsp_a1);
        wsp_a2=wsp_a(bx,by,cx,cy);
        wsp_b2=wsp_b(bx,by,wsp_a2);
        meet_point(x,y,wsp_a1,wsp_b1,wsp_a2,wsp_b2);
        if(inside_interval(x,x_beg,x_end))
           if(inside_interval(y,y_beg,y_end))
            return 1;
    }
    return 0;

}

bool Quadrangle::are_points_alrigth(Point& a, Point& b, Point &c, Point& d)
{
if(any_collinear(a,b,c,d))
    return false;

if(are_lines_crossed(a,b,c,d))
    return false;
else
    return true;
}

void Quadrangle::get_points(double& ax, double& ay, double& bx, double& by, double& cx, double& cy, double& dx, double& dy)
{
    ax=A.return_x();
    ay=A.return_y();
    bx=B.return_x();
    by=B.return_y();
    cx=C.return_x();
    cy=C.return_y();
    dx=D.return_x();
    dy=D.return_y();

}

void Quadrangle::move_quadrangle(double x,double y) // moving whole quadrangle
{
    A.move_point(x,y);
    B.move_point(x,y);
    C.move_point(x,y);
    D.move_point(x,y);
    mid.move_point(x,y) ;
    std::cout<<"Czworokat przesuniety o "<<x<<" "<<y<<std::endl;
}

void Quadrangle::print_quadrangle () //printing points of quadrangle
{
    A.print_point();
    B.print_point();
    C.print_point();
    D.print_point();
}

bool Quadrangle::set_quadrangle(double a,double b,double c,double d,double e,double f,double g,double h)
{
    bool alright ;
    A.set_point(a,b) ;
    B.set_point(c,d) ;
    C.set_point(e,f) ;
    D.set_point(g,h) ;
    alright=are_points_alrigth(A,B,C,D) ;
    if(alright==false)
        return false;
    srodek();
    return true;
}
// function used for tests similar to the previous, but without getting variables in function

void Quadrangle::srodek ()
{
	double x1,x2,y1,y2;
	double a1,a2,b1,b2;
	double x, y ;
	int c=0 ; // zwyczajna zmienna ktora zmienia wartosc gdy wykres jest pionowy
	double ax,bx,cx,dx,ay,by,cy,dy ;
	get_points(ax,ay,bx,by,cx,cy,dx,dy);

	x1=middle (ax,bx);
	y1=middle (ay,by);
	x2=middle (cx,dx);
	y2=middle (cy,dy);

	a1=wsp_a (x1,y1,x2,y2);
	b1=wsp_b (x1,y1,a1);

	x1==x2 ? x=x1,c=1:c=c  ;

	// wspolczynniki pierwszej funkcji oraz sprawdzenie czy wykresem nie jest linia pionowa

	x1=middle (bx,cx);
	y1=middle (by,cy);
	x2=middle (ax,dx);
	y2=middle (ay,dy);

	x1==x2 ? x=x1,c=2:c=c ;

	a2=wsp_a (x1,y1,x2,y2);
	b2=wsp_b (x1,y1,a2);

	// wspolczynniki drugiej funkcji oraz sprawdzenie czy wykresem nie jest linia pionowa

	if (c==1)
	{
		y=a2*x+b2;
		mid.set_point(x,y);
		return ;
	}
	else if (c==0)
	{
        x=(b2-b1)/(a1-a2);
	}
	y=a1*x+b1;
    mid.set_point(x,y);

	return ;
}

void Quadrangle::point_rotation(Point &a, Point &b, double fi)
{
	double x_pom1,y_pom1,x_pom2,y_pom2;
	double ax,bx,ay,by ;
	ax=a.return_x();
	bx=b.return_x();
	ay=a.return_y();
	by=b.return_y();
	x_pom1=ax-bx;
	y_pom1=ay-by;
	x_pom2=x_pom1*cos(fi)-y_pom1*sin(fi);
	y_pom2=x_pom1*sin(fi)+y_pom1*cos(fi);
	a.set_point(x_pom2+bx,y_pom2+by) ;
}
//rotation point a around point b

void Quadrangle::quadrangle_rotation(double fi)
{
	point_rotation (A,mid,fi);
	point_rotation (B,mid,fi);
	point_rotation (C,mid,fi);
	point_rotation (D,mid,fi);

}

void Quadrangle::graduation_point (Point &a, Point &b, double l)
{
    double ax,bx,ay,by ;
    ax=a.return_x();
	bx=b.return_x();
	ay=a.return_y();
	by=b.return_y();
	ax=bx-(bx-ax)*l;
	ay=by-(by-ay)*l;
	a.set_point(ax,ay);
}

void Quadrangle::graduation_quadrangle(double l)
{
    graduation_point (A,mid,l);
    graduation_point (B,mid,l);
	graduation_point (C,mid,l);
	graduation_point (D,mid,l);
}

double Quadrangle::quadrangle_circuit()
{
    double ax,bx,cx,dx,ay,by,cy,dy,sum ;
    double a,b,c,d;
	get_points(ax,ay,bx,by,cx,cy,dx,dy);
	a=segment_lenth(ax,ay,bx,by) ;
	b=segment_lenth(bx,by,cx,cy) ;
	c=segment_lenth(cx,cy,dx,dy) ;
	d=segment_lenth(dx,dy,ax,ay) ;
    sum=a+b+c+d;
    return sum;
}

double Quadrangle::quadrangle_area ()
{
    double ax,bx,cx,dx,ay,by,cy,dy; //wspolrzedne punktow
    double ab,bc,cd,da,ac,bd; //dlugosci bokow
    double P1,P2; //pomocnicze pola
    double PC1,PC2; // pola koncowe do porownania
    get_points(ax,ay,bx,by,cx,cy,dx,dy) ;
	//wspolrzedne punktow
	ab=segment_lenth(ax,ay,bx,by) ;
	bc=segment_lenth(bx,by,cx,cy) ;
	cd=segment_lenth(cx,cy,dx,dy) ;
	da=segment_lenth(dx,dy,ax,ay) ;
	ac=segment_lenth(ax,ay,cx,cy) ;
	bd=segment_lenth(bx,by,dx,dy) ;
	//dlugosci odcinkow laczacy poszczegolne punkty
    P1=heron(ab,bc,ac);
    P2=heron(ac,cd,da);
    PC1=P1+P2;
    //policzono pole wzglêdem pierwszej z przekatnych czworokata
    P1=heron(bc,cd,bd);
    P2=heron(ab,bd,da);
    PC2=P1+P2;
    //policzono pole wzglêdem drugiej z przekatnych czworokata
    //licze dwa razy ze wzgledu na mozliwosc wyst¹pienia czworokatow wkleslych
    return std::min(PC1,PC2) ;
}

void Quadrangle::one_point_symmetry(double& ax, double& ay,double a, double b, double a_pom)
{
    double x,y;
    double b_pom;
    b_pom=wsp_b(ax,ay,a_pom);
    meet_point(x,y,a,b,a_pom,b_pom);
    ax=ax+(x-ax)*2;
    ay=ay+(y-ay)*2;
}

void Quadrangle::quadrangle_line_symmetry(double a, double b)
{
    if(a==0)
    {
        quadrangle_special_symmetry(b) ;
        return ;
    }
    double ax,bx,cx,dx,midx,ay,by,cy,dy,midy;
    double a_pom;

    midx=mid.return_x();
    midy=mid.return_y();

    get_points(ax,ay,bx,by,cx,cy,dx,dy);
    //pobranie punktow
    a_pom=-(1/a);

    one_point_symmetry(ax,ay,a,b,a_pom);
    A.set_point(ax,ay);
    //
    one_point_symmetry(bx,by,a,b,a_pom);
    B.set_point(bx,by);

    one_point_symmetry(cx,cy,a,b,a_pom);
    C.set_point(cx,cy);

    one_point_symmetry(dx,dy,a,b,a_pom);
    D.set_point(dx,dy);

    one_point_symmetry(midx,midy,a,b,a_pom);
    mid.set_point(midx,midy);
}

void Quadrangle::quadrangle_special_symmetry(double b)
{
    double ax,bx,cx,dx,midx,ay,by,cy,dy,midy;

    midx=mid.return_x();
    midy=mid.return_y();

    get_points(ax,ay,bx,by,cx,cy,dx,dy);
    ay=ay+(b-ay)*2 ;
    by=by+(b-by)*2 ;
    cy=cy+(b-cy)*2 ;
    dy=dy+(b-dy)*2 ;
    midy=midy+(b-midy)*2 ;
    A.set_point(ax,ay);
    B.set_point(bx,by);
    C.set_point(cx,cy);
    D.set_point(dx,dy);
    mid.set_point(midx,midy);
}

bool Quadrangle::crossing_segments_points(double& x, double& y, Point& a, Point& b, double cx, double cy, double dx, double dy)
{
    double ax,bx,ay,by;
    double wsp_a1,wsp_b1,wsp_a2,wsp_b2;
    double x_pom, y_pom;
    double x_beg,x_end,y_beg,y_end;
    bool is_interval_1,is_interval_2;

    ax=a.return_x();
    ay=a.return_y();
    bx=b.return_x();
    by=b.return_y();

    is_interval_1=numerical_interval(x_beg,x_end,ax,bx,cx,dx);
    is_interval_2=numerical_interval(y_beg,y_end,ay,by,cy,dy);

    if(is_interval_1==1 && is_interval_2==1)
    {
        ///przypadek szczegolny
        if(ax==bx)
        {
            wsp_a2=wsp_a(cx,cy,dx,dy);
            wsp_b2=wsp_b(cx,cy,wsp_a2);
            y_pom=x_beg*wsp_a2+wsp_b2;
            double is_inside_interval;
            is_inside_interval=inside_interval(y_pom,y_beg,y_end);
            if(is_inside_interval)
            {
                x=x_beg;
                y=y_pom;
                return true;
            }
            return false;
        }
        if(cx==dx)
        {
            wsp_a1=wsp_a(ax,ay,bx,by);
            wsp_b1=wsp_b(ax,ay,wsp_a1);
            y_pom=x_beg*wsp_a1+wsp_b1;
            double is_inside_interval;
            is_inside_interval=inside_interval(y_pom,y_beg,y_end);
            if(is_inside_interval)
            {
                x=x_beg;
                y=y_pom;
                return true;
            }
            return false;
        }
        ///koniec przypadku szczegolnego

        wsp_a1=wsp_a(ax,ay,bx,by);
        wsp_b1=wsp_b(ax,ay,wsp_a1);
        wsp_a2=wsp_a(cx,cy,dx,dy);
        wsp_b2=wsp_b(cx,cy,wsp_a2);

        meet_point(x_pom,y_pom,wsp_a1,wsp_b1,wsp_a2,wsp_b2);

        double is_inside_interval_1,is_inside_interval_2;
        is_inside_interval_1=inside_interval(x_pom,x_beg,x_end);
        is_inside_interval_2=inside_interval(y_pom,y_beg,y_end);

        if(is_inside_interval_1 && is_inside_interval_2)
        {
            x=x_pom;
            y=y_pom;
            return true;
        }
    }
    return false ;
}

void Quadrangle::one_segment_crossing(Point& a, Point& b, double ax2, double ay2, double bx2, double by2, double cx2, double cy2, double dx2, double dy2)
{
    double x,y;
    bool are_crosed;
    are_crosed=crossing_segments_points(x,y,a,b,ax2,ay2,bx2,by2);
    if(are_crosed)
    {
        std::cout<<std::fixed<<std::setprecision(2)<<"X: "<<x<<" Y: "<<y<<std::endl;
    }
    are_crosed=crossing_segments_points(x,y,a,b,bx2,by2,cx2,cy2);
    if(are_crosed)
    {
        std::cout<<std::fixed<<std::setprecision(2)<<"X: "<<x<<" Y: "<<y<<std::endl;
    }
    are_crosed=crossing_segments_points(x,y,a,b,cx2,cy2,dx2,dy2);
    if(are_crosed)
    {
        std::cout<<std::fixed<<std::setprecision(2)<<"X: "<<x<<" Y: "<<y<<std::endl;
    }
    are_crosed=crossing_segments_points(x,y,a,b,dx2,dy2,ax2,ay2);
    if(are_crosed)
    {
        std::cout<<std::fixed<<std::setprecision(2)<<"X: "<<x<<" Y: "<<y<<std::endl;
    }

}

void Quadrangle::common_points(Quadrangle &kw2)
{
    double ax2,bx2,cx2,dx2,ay2,by2,cy2,dy2;
    kw2.get_points(ax2,ay2,bx2,by2,cx2,cy2,dx2,dy2);
    std::cout<<std::endl<<"Punkty wspolne tych dwoch czworokatow"<<std::endl;
    one_segment_crossing(A,B,ax2,ay2,bx2,by2,cx2,cy2,dx2,dy2);
    one_segment_crossing(B,C,ax2,ay2,bx2,by2,cx2,cy2,dx2,dy2);
    one_segment_crossing(C,D,ax2,ay2,bx2,by2,cx2,cy2,dx2,dy2);
    one_segment_crossing(D,A,ax2,ay2,bx2,by2,cx2,cy2,dx2,dy2);


}

